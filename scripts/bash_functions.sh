#Works as extension to the bash profile

function scripts() {
    echo "script functions available: "
    echo "mkd() <folder>"
    echo "exchangeOrigin <newOrigin> (used for git p)"
    echo "rmd <1.DirectoryNameToBeDeleted> <2.DirectoryNameToBeDeleted>"
    echo "mkd <directoryNameToBeCreated>"
    echo "cpd <directoryToCopy> <destinationDirectory>"
    echo ".. (same as cd ..)"
    echo "cl <directoryName>"
    echo "httpserver <port>"
    echo "dkexec <containerID> (used to exec bash)"
    echo "dkrm <containerID> (stops and deletes a container)"
    echo "removeRemoteBranch <BranchToDelte>"
    echo "switchJavaVersion"
}

# UTILITY alias functions
cpd() {
    if [ -z $1 ];
    then
        echo "Parameters are: cpd <directoryToCopy> <destinationDirectory>"
    else
        if [ -z $2 ];
        then
            echo "No destination directory given"
        else
            cp -r $1 $2
        fi
    fi
}

mkd() {
    mkdir -p $1 && cd $1
}

rmd() {
    rm -rf $1
    if [ -z $2 ];
    then
        rm -rf $2
    fi
}

cl() {
    cd $1 && ls -latrghc
}

httpserver()
{
    python -m SimpleHTTPServer $1
}

recreateDB()
{
    dropdb $1
    createdb $1
    cat $2 | psqlWithoutT $1
}

switchJavaVersion()
{
    update-java-alternatives --list
}

# GIT alias functions
exchangeOrigin()
{
    if [ -z $1 ]
    then
        echo "enter the new Origin "
    else
        git remote rm origin
        git remote add origin $1
        git pull
        git branch --set-upstream-to=origin/master master
    fi
}

removeRemoteBranch()
{
    if [ -z $1 ]
    then
        echo "enter the branch to delete remote"
    else
        git push origin --delete $1
    fi
}
